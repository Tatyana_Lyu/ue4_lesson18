#ifndef __STACK_H__
#define __STACK_H__

template <class T>
class Stack
{
private:
	T* container = nullptr;
	int capacity = 1;
	size_t ind = 0;
	size_t size = 0;

	void CopyContainer(T* copy_from, T* copy_to)
	{
		for (int i = 0; i < capacity; i++)
		{
			copy_to[i] = copy_from[i];
		}
	}

public:
	Stack() 
	{ 
		container = new T[capacity]; 
	}

	Stack(const int capacity_) 
	{ 
		container = new T[capacity_]; 
	}

	~Stack() { 
		if (container)
		delete[] container; 
	}

	int GetLength() { return size; }

	void Push(const T& element)
	{
		if (ind < capacity)
		{
			container[ind++] = element;
			size++;
		}
		else
		{
			T* temp_container = new T[2 * capacity];
			CopyContainer(container, temp_container);
			delete[] container;
			container = temp_container;
			capacity *= 2;

			container[ind++] = element;
			size++;
		}
	}

	bool Pop(T& val)
	{
		if (size)
		{
			size--;
			val = container[--ind];
			return true;
		}

		return false;		
	}
};
#endif // __STACK_H__
