#include <iostream>

#include "Stack.hpp"

int main()
{
	Stack<int> stack;

	for (int i = 0; i < 10; i++)
	{
		stack.Push(i + 1);
	}

	int element;
	while (stack.Pop(element))
	{
		std::cout << element << " " << element << std::endl;
	}

	system("PAUSE");

	return 0;
}
